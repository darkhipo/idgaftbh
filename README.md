# idgaftbh

- I am not concerned about the nature of a valuation made based on collective indifference of a metric itself defined to be subjective and arbitrary.

## Purpose

- I'm not playing this game; this extention can do it for me.

## Reference

Game site: https://www.scrumpoker-online.org

## Dependencies

```
npm install --global web-ext
```

## Packaging

```
web-ext build --source-dir="idgaftbh"  --overwrite-dest --artifacts-dir="." --filename idgaftbh.zip
```

## Install

- follow the [directions here](https://extensionworkshop.com/documentation/develop/temporary-installation-in-firefox) to install.

!["screenshot"](/assets/images/tmp-ext-1.png "screenshot")
