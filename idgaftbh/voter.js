console.log("START");
const DEFAULT_VOTE = 3;

// Callback function to execute when mutations are observed
async function mutationHandler(mutationList, observer) {
  console.log("Change detected");
  let avgVote = 0;
  const table = document.querySelector(".mat-table");
  if (!table) {
    console.log("TABLE IS NULL");
    return;
  }
  let participantCount = table.rows.length - 1;

  for (let ridx = 1; ridx < table.rows.length; ridx++) {
    // Skip header
    const row = table.rows[ridx];
    const val = row.cells[1].querySelector(".flip-card-back").innerText;
    const rval = parseInt(val);

    console.log(`1-ROW[${ridx}]=${val}`);
    if (!isNaN(rval)) {
      console.log(`2-ROW[${ridx}]=${rval}`);
      if (val) {
        avgVote += rval + (Math.random() * (0.001 - 0.0001) + 0.0001);
      }
    } else {
      participantCount -= 1;
    }
  }
  avgVote = avgVote / participantCount;
  console.log(`1-AVG=${avgVote}`);
  const cardCount =
    document.querySelector("div.flex-container").children.length;
  for (let cidx = 0; cidx < cardCount; cidx++) {
    const card = document.querySelector("div.flex-container").children[cidx];
    const cVal = card.querySelector("button>span>div .middle").innerHTML;
    const isSelected = card.className.includes("selected");
    if (isSelected && parseInt(cVal)) {
      participantCount -= 1;
      avgVote =
        (avgVote * (participantCount + 1) - parseInt(cVal)) / participantCount;
      break;
    }
    if (isSelected && !parseInt(cVal)) {
      participantCount -= 1;
      avgVote = (avgVote * (participantCount + 1)) / participantCount;
      break;
    }
  }
  if (participantCount < 1) {
    participantCount = 1;
    avgVote = DEFAULT_VOTE;
  }
  console.log(`PARTICIPANT COUNT=${participantCount}`);
  avgVote = avgVote;
  console.log(`2-AVG=${avgVote}`);

  let minDeltaCursor = {
    cidx: -1,
    cVal: 9999,
  };
  let noContinue = false;
  for (let cidx = 0; cidx < cardCount; cidx++) {
    const card = document.querySelector("div.flex-container").children[cidx];
    const cVal = card.querySelector("button>span>div .middle").innerHTML;
    const isSelected = card.className.includes("selected");
    console.log(`1-CARD[${cidx}]=${cVal}`);

    if (avgVote.toString() === cVal) {
      console.log(`AVG-CARD[${cidx}]=${cVal}`);
      if (!isSelected) {
        card.click();
        noContinue = true;
        console.log("click 1");
        return;
      }
    }
  }
  if (noContinue) {
    console.log("1-exit no continue");
    return;
  }
  for (let cidx = 0; cidx < cardCount; cidx++) {
    const card = document.querySelector("div.flex-container").children[cidx];
    const cVal = card.querySelector("button>span>div .middle").innerHTML;
    console.log(`2-CARD[${cidx}]=${cVal}`);
    const cValInt = parseInt(cVal);

    const prevDiff = Math.abs(avgVote - minDeltaCursor.cVal);
    const currDiff = Math.abs(avgVote - cValInt);
    console.log(
      `CMP(${minDeltaCursor.cidx},${minDeltaCursor.cVal})~(${cidx},${cValInt})-${avgVote}-(${prevDiff},${currDiff})`
    );
    console.log(`2-AVG`, avgVote);
    if (cValInt === avgVote) {
      const card = document.querySelector("div.flex-container").children[cidx];
      const isSelected = card.className.includes("selected");
      if (!isSelected) {
        card.click();
        noContinue = true;
        console.log("click 2");
        return;
      }
    }
    if (cValInt && currDiff < prevDiff) {
      minDeltaCursor = { cidx, cVal };
      console.log(`SET CVAL[${cidx}]=${cVal}`);
    }
  }
  if (noContinue) {
    console.log("2-exit no continue");
    return;
  }
  const card =
    document.querySelector("div.flex-container").children[minDeltaCursor.cidx];
  const isSelected = card.className.includes("selected");
  if (!isSelected) {
    card.click();
    console.log("click 3", minDeltaCursor.cidx, minDeltaCursor.cVal);
    return;
  }
}

const doBoot = setTimeout(function boot() {
  const html = document.querySelector("html");
  const voteTable = document.querySelector(".mat-table");
  console.log("try boot");
  if (html) {
    console.log("yes boot");
    // Monitor mutations in page.
    const observer = new MutationObserver(mutationHandler);
    observer.observe(html, {
      attributes: true,
      childList: true,
      subtree: true,
    });
    // Cleanup. On uload?
    // observer.disconnect();
  } else {
    console.log("no boot");
    setTimeout(doBoot, 5);
  }
}, 5);

window.onload = doBoot;
